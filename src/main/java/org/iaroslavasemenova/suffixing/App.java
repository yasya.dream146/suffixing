package org.iaroslavasemenova.suffixing;
import java.io.IOException;

public class App {

    public static void main(String[] array) throws IOException {
        if (array.length != 0) {
            Suffixing.suffix(array[0]);
        }
    }

}
