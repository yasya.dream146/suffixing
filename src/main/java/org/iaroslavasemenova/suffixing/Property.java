package org.iaroslavasemenova.suffixing;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Property {
    private static final Logger logger = Logger.getLogger(App.class.getName());

    public static String mode;
    public static String suffix;
    public static String files;

    public void getProperty(String configFile) throws IOException {
        File file = new File(configFile);

        Properties properties = new Properties();
        properties.load(new FileReader(file));

        this.mode = properties.getProperty("mode");
        if (!this.mode.toLowerCase().equals("copy") && !this.mode.toLowerCase().equals("move")) {
            logger.log(Level.SEVERE, "Mode is not recognized: " + this.mode);
            return;
        }
        this.suffix = properties.getProperty("suffix");
        if (this.suffix == null || this.suffix.equals("")) {
            logger.log(Level.SEVERE, "No suffix is configured");
            return;
        }
        this.files = properties.getProperty("files");
        if (this.files == null || this.files.equals("")){
            logger.log(Level.WARNING, "No files are configured to be copied/moved.");
            return;
        }
    }
}
