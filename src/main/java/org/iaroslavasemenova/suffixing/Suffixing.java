package org.iaroslavasemenova.suffixing;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Suffixing {
    private static final Logger logger = Logger.getLogger(App.class.getName());

    public static void suffix(String configFile) throws IOException {
        Property property = new Property();
        property.getProperty(configFile);
        String mode = property.mode.toLowerCase();

        if (property.files != null) {
            String[] files = property.files.split(":");
            for (String filePath : files) {
                File file = new File(filePath);
                if (!file.exists()) {
                    logger.log(Level.SEVERE, "No such file: " + file);
                    continue;
                }
                Path source = Paths.get(file.getPath());
                String[] fileName = file.getPath().split("\\.");
                Path dest = Paths.get(fileName[0] + property.suffix + "." + fileName[1]);
                if (mode.equals("copy")) {
                    Files.copy(source, dest);
                    logger.log(Level.INFO, file + " -> " + dest);
                } else if (mode.equals("move")) {
                    Files.move(source, dest);
                    logger.log(Level.INFO, file + " => " + dest);
                }
            }
        }
    }
}

